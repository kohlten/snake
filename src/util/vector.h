#ifndef VECTOR_H
#define VECTOR_H

typedef struct s_vector2f
{
	double x;
	double y;
} t_vector2f;

typedef struct s_vector2i
{
	signed long x;
	signed long y;
} t_vector2i;

typedef struct s_vector2u
{
	unsigned long x;
	unsigned long y;
}	t_vector2u;

#endif