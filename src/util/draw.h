#ifndef DRAW_H
#define DRAW_H
#include <SDL.h>

void DrawCircle(SDL_Renderer *Renderer, int32_t _x, int32_t _y, int32_t radius);
#endif